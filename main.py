# -*- coding: utf-8 -*-
import datetime
import pymongo
from bson.objectid import ObjectId
from bson.errors import InvalidId
from flask import (Flask, render_template, request, flash, redirect, url_for, g,
                   abort)
from markdown2 import Markdown
from pygments.formatters import get_formatter_by_name

app = Flask(__name__)

DEBUG = True
SECRET_KEY = 'dev secret key'
app.config.from_object(__name__)


@app.before_request
def before_request():
    if not hasattr(g, 'db'):
        g.db = pymongo.MongoClient()['mdblog']


@app.teardown_request
def teardown_request(error):
    if hasattr(g, 'db'):
        g.db.connection.close()


@app.route('/')
def index():
    css = get_formatter_by_name('html').get_style_defs('.highlight')
    entries = g.db['entries'].find().sort('created_at', pymongo.DESCENDING)
    return render_template('index.html', entries=entries, css=css)


@app.route('/entry', methods=['POST'])
@app.route('/entry/<entry_id>')
def entry(entry_id=None):
    if request.method == 'GET':
        try:
            new_entry = g.db['entries'].find_one({'_id': ObjectId(entry_id)})
            css = get_formatter_by_name('html').get_style_defs('.highlight')
            return render_template('entry.html', entry=new_entry, css=css)
        except InvalidId:
            abort(404)
    else:
        md = Markdown(extras={
            'code-friendly': 1,
            'fenced-code-blocks': {
                'cssclass': 'highlight',
                'linenos': True,
            },
        })

        title = request.form.get('title')
        content = request.form.get('content')
        converted_content = md.convert(content)
        new_entry = {
            'title': title,
            'content': content,
            'converted_content': converted_content,
            'created_at': datetime.datetime.now(),
        }
        g.db['entries'].insert(new_entry)
        flash('new entry posted.', 'success')
        return redirect(url_for('index'))


@app.template_filter('timestamp')
def timestamp(t):
    """ output a timestamp like "* minutes ago"
    """
    diff = datetime.datetime.now() - t
    if diff < datetime.timedelta(minutes=1):
        return '{seconds} seconds ago'.format(seconds=diff.seconds)
    elif diff < datetime.timedelta(hours=1):
        return '{minutes} minutes ago'.format(minutes=diff.seconds//60)
    elif diff < datetime.timedelta(days=1):
        return '{hours} hours ago'.format(hours=diff.seconds//(60*60))
    elif diff < datetime.timedelta(days=365):
        return '{days} days ago'.format(days=diff.days)
    else:
        return '{years} years ago'.format(years=diff.days//365)


if __name__ == '__main__':
    app.run(port=app.config.get('PORT', 8000))